package RMICalc;

import java.rmi.registry.Registry;

public class CalcServer2 {
	public static void main(String[] args) {
		try {
			Registry r = java.rmi.registry.LocateRegistry.createRegistry(6789);/*Create Registry*/
			r.rebind("Calc", new CalcRmi()); /*Binding the implementation class*/
			System.out.println("Servidor pronto!");
		} catch (Exception e) {
			System.out.println("Server not connected " + e);
		}
	}
}
