package RMICalc;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class CalcRmi extends UnicastRemoteObject implements CalcInterface {

	public CalcRmi() throws RemoteException{
		int x, y;
	}
	
	@Override
	public int add(int x, int y) throws RemoteException {
		System.out.println("Soma requisitada! Somando " + x + " e " + y);
		return x+y;
	}

	@Override
	public int sub(int x, int y) throws RemoteException {
		return x-y;
	}

	@Override
	public int mul(int x, int y) throws RemoteException {
		return x*y;
	}

	@Override
	public int div(int x, int y) throws RemoteException {
		return x/y;
	}

	@Override
	public String add2(int x, int y) throws RemoteException {
		return ("Somados " + x + " e " + y + ", totalizando " + (x+y));
	}

}
