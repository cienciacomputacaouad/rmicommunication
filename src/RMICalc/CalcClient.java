package RMICalc;

import java.rmi.Naming;
import java.util.Scanner;

public class CalcClient {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		try {
			CalcInterface c = (CalcInterface)Naming.lookup("//127.0.0.1/Calc"); /*The object returned 
			by lookup depends on the naming system and data associated with it so we typecast it*/
			
			int choice = sc.nextInt();
			int x, y;
			switch (choice) {
			case 1:
			{
				System.out.println("Entre com x e y:");
				x = sc.nextInt();
				y = sc.nextInt();
				System.out.println(c.add(x, y));
				break;
			}
			case 2:
			{
				System.out.println("Entre com x e y:");
				x = sc.nextInt();
				y = sc.nextInt();
				System.out.println(c.sub(x, y));
				break;
			}
			case 3:
			{
				System.out.println("Entre com x e y:");
				x = sc.nextInt();
				y = sc.nextInt();
				System.out.println(c.mul(x, y));
				break;
			}
			case 4:
			{
				System.out.println("Entre com x e y:");
				x = sc.nextInt();
				y = sc.nextInt();
				System.out.println(c.div(x, y));
				break;
			}
			case 5:
			{
				System.out.println("Entre com x e y:");
				x = sc.nextInt();
				y = sc.nextInt();
				System.out.println(c.add2(x, y));
				break;
			}
			default:
				break;
			}
		} catch (Exception e) {
			System.out.println("Erro" + e);
		}
	}
}
