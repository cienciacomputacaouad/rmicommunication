package RMICalc;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface CalcInterface extends Remote { /* Interface is remote interface so we extend remote class
 	by extending remote class the interface identifies itself as an interface whose methods can be invoked
 	by other jvm*/
	public int add(int x, int y) throws RemoteException;
	public String add2(int x, int y) throws RemoteException;
	public int sub(int x, int y) throws RemoteException;
	public int mul(int x, int y) throws RemoteException;
	public int div(int x, int y) throws RemoteException;
}
